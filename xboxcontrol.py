import os
import pygame
import serial
import time

from recordtype import recordtype

debug = False

os.environ["SDL_VIDEODRIVER"] = "dummy"

pygame.init()

controller = pygame.joystick.Joystick(0)
controller.init()

JoystickState = recordtype("JoystickState", "lts_x, lts_y, rts_x, rts_y, lt, rt")
js = JoystickState(0, 0, 0, 0, -1, -1)

sc = serial.Serial("/dev/ttyACM0", 9600, timeout=5)

time.sleep(5)

def send_command():
    drt = int(round(js.lts_x * 7))
    fwd = int(round((js.rt + 1) * 3.5))
    bwd = int(round((js.lt + 1) * 3.5))

    a, b  = 0, 0

    if drt > 0:
        a = drt
    elif drt < 0:
        a = 16 + drt

    if fwd > 0 and bwd == 0:
        b = fwd
    elif bwd > 0 and fwd == 0:
        b = 16 - bwd

    cmd = bytearray([0x0, a, b])
    sc.write(cmd)

    if debug:
        print "Command sent: %s" % " ".join(hex(b) for b in cmd)

    res = bytearray(sc.read()) # Must be in a bytearray to work

    if debug:
        print "Return code: %s" % hex(res[0])

    return res[0] == 0x1


def read_joystick_state(js):
    for event in pygame.event.get():
        if event.type == pygame.JOYAXISMOTION:

            # Left thumb stick
            if event.axis == 0:
                js.lts_x = event.value
            elif event.axis == 1:
               js.lts_y = event.value

            # Left trigger
            elif event.axis == 2:
                js.lt = event.value

            # Right thumb stick
            elif event.axis == 3:
                js.rts_x = event.value
            elif event.axis == 4:
                js.rts_y = event.value
           # Right trigger
            elif event.axis == 5:
                js.rt = event.value

        #elif event.type == pygame.JOYHATMOTION:
        #    print event

        #elif event.type == pygame.JOYBALLMOTION:
        #    print event

        #elif event.type == pygame.JOYBUTTONDOWN:
        #    print event
        #elif event.type == pygame.JOYBUTTONUP:
        #    print event


# Main loop
while True:
    read_joystick_state(js)
    send_command()
#    if not send_command():
#        break

#include <legopowerfunctions.h>

LEGOPowerFunctions lego(13);

char command[2];

int mode;
int channel;
int outputA;
int outputB;

void setup()
{
  Serial.begin(9600); // Bits per second (baud)
  Serial.setTimeout(5000); // 5 seconds
  Serial.write((byte) 0x0); // OK/Ready
}

void loop()
{
  if (Serial.readBytes(command, 2) > 0)
  {
    mode    = int((unsigned char) command[0] >> 4);
    channel = int(command[0] & 0x0f);
    outputA = int((unsigned char) command[1] >> 4);
    outputB = int(command[1] & 0x0f);

    lego.ComboPWM(outputA, outputB, channel);

    Serial.write((byte) 0x0); // OK/Ready
  }
}

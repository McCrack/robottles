# Robottles
LEGO powered robot vehicle controlled by a Raspberry Pi and Arduino Uno.

## Requirements
* apt-get install python-serial
* apt-get install python-pygame
* pip install recordtype
* pip install ino

## Checklist
	[X] Control LEGO Power Functions from Raspberry Pi through Arduino.
	[X] Control LEGO Crawler using Xbox 360 wireless controller.
	[ ] Control LEGO Excavator (with two PF receivers) using Xbox 360 wireless controller.
	[ ] On remote computer get live video (no delay) from Raspberry Pi.
	[ ] Control LEGO Crawler using Xbox 360 controller from a remote Raspberry Pi.
	[ ] Add microphone to Raspberry Pi and control Robottles using voice commands.
	[ ] Add ultra sound distance sensor on Robottles to prevent frontal collisions.
	[ ] Add robotic arm to Robotles with camera mounted on it.
	[ ] Add speaker to Robottles in order to enable speech.
	[ ] Turn on/off Raspberry Pi from Arduino which is always powered on.
	[ ] Add PIR and/or light sensor on Robottles to wake up Raspberry Pi on movement activity.
	[ ] Add microphone on Robottles to wake up Raspberry Pi on high sounds.
	[ ] Control IR devices (lights, receiver, projector) through voice commands.
	[ ] Enable recognition of visual objects using camera.
	[ ] Make Robottles able to seek for objects.
	[ ] Add distance sensor on robotic arm and do automatic pick up of objects.
	[ ] Upgrade Robottles with two XL motors to increase speed.
	[ ] Setup SaltStack in order install Robottles automatically from HG repository.
	[ ] Add temperature and humidity sensors and let Robottles speak climatic readings.
	[ ] Add dust sensor and let Robottles start Samsung NaviBot (via IR) when needed.

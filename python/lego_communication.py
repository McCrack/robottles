import serial


debug = False

prev_cmd = None

# Establish serial connection to Arduino"
sc = serial.Serial("/dev/ttyACM0", 9600, timeout=5)
print "Connected to Arduino"

# Wait for Arduino to restart and send ready code
if bytearray(sc.read())[0] == 0:
    print "Arduino ready"


def send_lego_command(mode, channel, output_a, output_b):
    cmd = bytearray([ (mode << 4) | (channel & 0x0f), (output_a << 4) | (output_b & 0x0f) ])

    global prev_cmd
    if prev_cmd and prev_cmd[1] + cmd[1] == 0:
        if debug:
            print "Ignoring command: %s" % " ".join(hex(b) for b in cmd)

        return False
       
    sc.write(cmd)

    if debug:
        print "Sent command: %s" % " ".join(hex(b) for b in cmd)

    res = bytearray(sc.read()) # Must be in a bytearray to work

    if debug:
        print "Return code: %s" % hex(res[0])

    prev_cmd = cmd

    return res[0] == 0 # Executed successfully

import os
import pygame
import pygame.camera

from recordtype import recordtype


os.environ["SDL_VIDEODRIVER"] = "dummy"

pygame.init()
#screen = pygame.display.set_mode((640, 560))
#pygame.display.set_caption("Robottles")

# Camera
#pygame.camera.init()
#camera = pygame.camera.Camera("/dev/video0", (640, 360))
#camera.start()

#image = pygame.surface.Surface(camera.get_size(), 0, screen)

#clock = pygame.time.Clock()
#while True:
    #for event in pygame.event.get():
    #    if event.type == pygame.QUIT:
    #        pygame.quit()

    #clock.tick(50)

    #if camera.query_image():
#    if True:
#        image = camera.get_image(image)

        #screen.fill([0, 0, 0])
#        screen.blit(image, (0, 0))

#        pygame.display.update()
        #pygame.display.flip()


# Controller

if pygame.joystick.get_count():
    controller = pygame.joystick.Joystick(0)
    controller.init()
    print "Initialized Xbox controller"
else:
    print "No Xbox controller found"


ControllerState = recordtype("ControllerState", "lts_x, lts_y, rts_x, rts_y, lt, rt")
cs = ControllerState(0, 0, 0, 0, -1, -1)

def read_controller_state():
    global cs

    for event in pygame.event.get():
        if event.type == pygame.JOYAXISMOTION:

            # Left thumb stick
            if event.axis == 0:
                cs.lts_x = event.value
            elif event.axis == 1:
               cs.lts_y = event.value

            # Left trigger
            elif event.axis == 2:
                cs.lt = event.value

            # Right thumb stick
            elif event.axis == 3:
                cs.rts_x = event.value
            elif event.axis == 4:
                cs.rts_y = event.value
           # Right trigger
            elif event.axis == 5:
                cs.rt = event.value

        #elif event.type == pygame.JOYHATMOTION:
        #    print event

        #elif event.type == pygame.JOYBALLMOTION:
        #    print event

        #elif event.type == pygame.JOYBUTTONDOWN:
        #    print event
        #elif event.type == pygame.JOYBUTTONUP:
        #    print event
        
    return cs
